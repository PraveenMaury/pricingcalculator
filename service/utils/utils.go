package utils

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"time"
)

type UtilContext struct {
	logger               *log.Logger
	BaseRates            map[int32]float64
	AgeFactors           map[int32]float64
	InsuranceGroupFactor []InsuranceGroupFactor
	LicenseLengthFactors []LicenseLengthFactor
	layoutISO            string
}

// PriceConfig the PriceConfig.json data is parsed in given format
type PriceConfig struct {
	BaseRates       []BaseRate             `json:"baseRates"`
	DriverAge       []AgeFactor            `json:"driverAge"`
	InsuranceGroups []InsuranceGroupFactor `json:"insuranceGroups"`
	LicenseLengths  []LicenseLengthFactor  `json:"licenseLengths"`
}

type BaseRate struct {
	Duration int32   `json:"duration"`
	Rate     float64 `json:"rate"`
}

type AgeFactor struct {
	Age int32 `json:"age"`
	// Factor 0 means decline
	Factor float64 `json:"factor"`
}

type InsuranceGroupFactor struct {
	Min int32 `json:"min"`
	Max int32 `json:"max"`
	// Factor 0 means decline
	Factor float64 `json:"factor"`
}

type LicenseLengthFactor struct {
	Min int32 `json:"min"`
	Max int32 `json:"max"`
	// Factor 0 means decline
	Factor float64 `json:"factor"`
}

// New: this will read the json Rateconfig file for price calculation  and give UtilContext
func New(logger *log.Logger, layoutISO string) (*UtilContext, error) {
	var priceConfig PriceConfig
	baseRates := make(map[int32]float64)
	ageFactors := make(map[int32]float64)

	// get the current working directory to read the PriceConfig file
	wd, err := os.Getwd()
	if err != nil {
		logger.Println(err)
	}
	folder := wd + string(os.PathSeparator) + "service" + string(os.PathSeparator) + "json" + string(os.PathSeparator)
	log.Println("Reading file PriceConfig.json from folder path :", folder)
	content, err := ioutil.ReadFile(folder + "PriceConfig.json")
	if err != nil {
		logger.Println("PriceConfig.json missing", err)
	}
	err = json.Unmarshal([]byte(content), &priceConfig)
	if err != nil {
		logger.Println("PriceConfig.json error parsing", err)
		return nil, err
	}
	for _, val := range priceConfig.BaseRates {
		baseRates[val.Duration] = val.Rate
	}
	for _, val := range priceConfig.DriverAge {
		ageFactors[val.Age] = val.Factor
	}

	return &UtilContext{logger: logger, BaseRates: baseRates, AgeFactors: ageFactors, InsuranceGroupFactor: priceConfig.InsuranceGroups, LicenseLengthFactors: priceConfig.LicenseLengths, layoutISO: layoutISO}, nil
}

// GetBaseRates will returns map with key as duration and value as base rate
func (utils *UtilContext) GetBaseRates() map[int32]float64 {
	return utils.BaseRates
}

// GetAgeFactor will return the factor value by given age , and will return error if factor value is 0.0 (Declined)
func (utils *UtilContext) GetAgeFactor(age int32) (float64, error) {
	if age >= 26 {
		return 1.000, nil
	}
	factor := utils.AgeFactors[age]
	if factor == 0.0 {
		return 0.0, errors.New("Declined:Age Factor")
	}
	return factor, nil
}

// GetInsuranceGroupFactor will return the factor value by given Group value ,will return error if factor value is 0.0 (Declined)
func (utils *UtilContext) GetInsuranceGroupFactor(group int32) (float64, error) {
	for _, val := range utils.InsuranceGroupFactor {
		if group >= val.Min && group <= val.Max {
			return val.Factor, nil
		}
	}
	return 0.0, errors.New("Declined:Insurance Group")
}

// GetLicenseLengthFactor will return the factor value n the basis of given year
func (utils *UtilContext) GetLicenseLengthFactor(year int32) float64 {
	for _, val := range utils.LicenseLengthFactors {
		if year >= val.Min && year < val.Max {
			return val.Factor
		}
	}
	return 0.950
}

// ParseTime format the given date string with provided layout
func (utils *UtilContext) ParseTime(date string) (time.Time, error) {
	dBirth, err := time.Parse(utils.layoutISO, date)
	if err != nil {
		return dBirth, err
	}
	return dBirth, nil
}

// Duration Calculates the duartion in year for given date
func (utils *UtilContext) Duration(bDate time.Time) int32 {
	today := time.Now()
	year, _, _ := today.Date()
	bYear, _, _ := bDate.Date()
	if today.Before(bDate) {
		return 0
	}
	age := year - bYear
	anniversary := bDate.AddDate(age, 0, 0)
	if anniversary.After(today) {
		age--
	}
	return int32(age)
}
