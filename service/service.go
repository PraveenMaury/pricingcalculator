package service

import (
	"log"
	"net/http"
	"os"
	"time"

	"pricingengine/service/app"
	"pricingengine/service/rpc"
	"pricingengine/service/utils"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

const (
	// used to parse the given date string
	layoutISO = "2006-01-02"
)

// Start begins a chi-Mux'd net/http server on port 3000
func Start() {
	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(middleware.Timeout(5 * time.Second))

	logger := log.New(os.Stderr, "price-chk-svc: ", log.Ldate|log.Ltime|log.Lshortfile)

	// create new utility context
	utilCtx, err := utils.New(logger, layoutISO)
	if err != nil {
		log.Fatal("Error occured", err)
	}
	rpc := rpc.RPC{
		App: app.New(utilCtx, logger),
	}

	r.Post("/generate_pricing", rpc.GeneratePricing)
	http.ListenAndServe(":3000", r)
}
