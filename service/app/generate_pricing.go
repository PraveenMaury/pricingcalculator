package app

import (
	"context"
	"log"
	"pricingengine/service/utils"

	"pricingengine"
)

type App struct {
	util   *utils.UtilContext
	logger *log.Logger
}

func New(utilCtx *utils.UtilContext, logger *log.Logger) *App {
	return &App{util: utilCtx, logger: logger}
}

// GeneratePricing will calculate how much a 'risk' be priced or if they should
// be denied.
func (a *App) GeneratePricing(ctx context.Context, input *pricingengine.GeneratePricingRequest) (*pricingengine.GeneratePricingResponse, error) {

	var rates []pricingengine.Rate

	dob, err := a.util.ParseTime(input.DateOfBirth)
	if err != nil {
		a.logger.Println("Error Parsing time", err.Error())
		return nil, err
	}
	lYear, err := a.util.ParseTime(input.LicenseHeldSince)
	if err != nil {
		a.logger.Println("Error Parsing time", err.Error())
		return nil, err
	}
	age := a.util.Duration(dob)
	licenseYear := a.util.Duration(lYear)

	//Calculate ageFactor , if age factor is 0 return error "Declined"
	ageFactor, err := a.util.GetAgeFactor(age)
	if err != nil {
		a.logger.Println("Declined due to age", age)
		return nil, err
	}

	//calculate insurance group factor , if group insurance facotr is 0 return error "Declined"
	insGrpFactor, err := a.util.GetInsuranceGroupFactor(input.InsuranceGroup)
	if err != nil {
		a.logger.Println("Declined due to group ", input.InsuranceGroup)
		return nil, err
	}
	//calculate LicenseYear factor
	liecenseYearFactor := a.util.GetLicenseLengthFactor(licenseYear)

	// getBaseRates for all the durations in seconds
	baseRates := a.util.GetBaseRates()

	// calculate all the pricing for each duration from the above calculated factors
	for key, val := range baseRates {
		rates = append(rates, pricingengine.Rate{Duration: key, RateInPence: val * ageFactor * insGrpFactor * liecenseYearFactor})
	}
	return &pricingengine.GeneratePricingResponse{Rates: rates}, nil
}
