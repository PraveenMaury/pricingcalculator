package pricingengine

// GeneratePricingRequest is used for generate pricing requests, it holds the
// inputs that are used to provide pricing for a given user.
type GeneratePricingRequest struct {
	DateOfBirth      string `json:"date_of_birth"`
	InsuranceGroup   int32  `json:"insurance_group"`
	LicenseHeldSince string `json:"license_held_since"`
	// TODO: populate me!
}

// GeneratePricingResponse - TODO: please document me :)
type GeneratePricingResponse struct {
	Rates []Rate `json:"rates"`
	// TODO: populate me!
}

type Rate struct {
	Duration    int32   `json:"duration"`
	RateInPence float64 `json:"rateInPence"`
}
